## isynth-cli run-data-config

Synthesize data in iSynth based on an iSynth data config

### Synopsis

Synthesize data in iSynth based on an iSynth data config.
Please note that this feature requires an iSynth server version >= 4.0.

The data config may be defined:
1. in a file stored on your local machine, or
2. in a file stored within your iSynth environment, or
3. inline (just as a string, not stored as a file)

```
isynth-cli run-data-config [flags]
```

### Examples

```bash
# Synthesize data using a data config stored inside your iSynth environment:
isynth-cli run-data-config --host <isynth host>:<isynth port> --env <isynth environment> --data-config data_configs/path/to/data_config.yml

# iSynth will tell you the ID of your order execution (which you may need later on once you want to provision the generated data into some target systems).
# You may save the order ID into a variable like this:

ORDER_ID=$(isynth-cli run-data-config --host <isynth host>:<isynth port> --env <isynth environment> --data-config data_configs/path/to/data_config.yml)

# Beware that this example shows the syntax for bash. This will not work in Powershell.
# If you are on Windows you may either use this example with WSL / Git Bash or you will need to adjust the syntax slightly to Powershell.

# Instead of executing a data config file which already exists in your iSynth environment, you may also  
# use a local data config file on your machine. iSynth CLI will then send the information from your local data config file to iSynth. 
isynth-cli run-data-config --host <isynth host>:<isynth port> --env <isynth environment> --local-data-config ./data-config/my_local_data-config.yml

# Lastly you can also use a string value containing the actual data config data in the iSynth format. All other explanations for usage of local data config files apply in this case as well.

# A data config in yaml format using a bash variable:
data_config_data='case_1:
		based_on: client_templates/individual_client
		number_of_copies: 20
		parameters:
		  param1: value1
		  param2: 2-CH
		  param3: 4
	  case_2:
		based_on: client_templates/couple_client
		parameters:
		  param1: value2
		modify:
		  Constellation:
			readonly: true'

isynth-cli run-data-config --host <isynth host>:<isynth port> --env <isynth environment> --data-config-data "$data_config_data"

# Please note the quotation marks around the variable $data_config_data.
# You may also use a json format (instead of yaml). You may also pass the content to the CLI without using a bash variable.
```

### Mandatory Flags

```
  -e, --env string    iSynth environment (default "")
      --host string   isynth base url (e.g. http://localhost:8010).
                      If you use https protocol to connect to iSynth you may specify the
                      certificates to be used by setting the environment variable
                      'SSL_CERT_FILE' (e.g.
                      SSL_CERT_FILE=/usr/local/share/ca-certificates/ca-all.pem).
                      Alternatively you may also use the directory version of this environment
                      variable SSL_CERT_DIR, (e.g.
                      SSL_CERT_DIR=/usr/local/share/ca-certificates/) (default "")
```

### Optional Flags

```
      --async                         If this flag is set then the iSynth order will be
                                      started asynchronously and the status will be polled
                                      regularly. 
                                      	This may help against timeout issues with long running
                                      orders. (default false). (default false)
      --async-poll-interval float32   Number of seconds between two status polls for an
                                      asynchronously running order (default 5)
      --data-config string            Path to the data config file within the iSynth
                                      environment (default "")
      --data-config-data string       Data config data as string. (default "")
      --local-data-config string      Path to the data config file locally on your machine.
                                      (default "")
```

### Technical Flags

```
      --dump-http-requests             Dump all raw HTTP requests and responses. Cannot be
                                       used together with '--silent' (default false)
      --first-retry-after int          Time until first retry in seconds. All further retries
                                       will use capped exponential backoff with jitter. (default 1)
      --force-http-1                   enforce HTTP/1.1 protocol even if the iSynth server
                                       support HTTP/2.0 (default false)
      --force-no-auth                  if set then isynth-cli will not send an authentication
                                       token to iSynth even if one has been set via login
                                       before (default false)
      --max-retries uint               maximal number of retries for REST calls in case of
                                       certain errors (default 1)
      --max-time-between-retries int   Max time between two retries in seconds (the 'capped'
                                       in capped exponential backoff with jitter). (default 30)
      --silent                         suppress ALL logging outputs besides errors. Cannot be
                                       used together with '--verbose' or
                                       '--dump-http-requests'. (default false)
      --time-between-retries int       DEPRECATED: Time until first retry in seconds. All
                                       further retries will use capped exponential backoff
                                       with jitter. Use '--first-retry-after' (default 0)
      --timeout int                    Timeout for all REST calls in seconds. Use 0 for no
                                       timeout (default 300)
      --verbose                        verbose output. Cannot be used together with
                                       '--silent'. (default false)
```

### Additional Options

```
  -h, --help   help for run-data-config (default false)
```

### SEE ALSO

* [isynth-cli](isynth-cli.md)	 - isynth-cli - a cli for invoking isynth functionality
###### Auto generated by itopia AG on 23-Feb-2025
