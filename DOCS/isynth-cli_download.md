## isynth-cli download

Download files from the iSynth environment

### Synopsis

Download either single files or entire folders (as tgz archive) from an iSynth environment.
	
WARNING: Please be aware that the download requests towards iSynth may result in VERY large responses from iSynth. As a 
consequence you should use --dump-http-request only temporarily if absolutely necessary for debugging purposes.

```
isynth-cli download [flags]
```

### Examples

```bash
# Download a single file from an iSynth environment to the local working directory:
isynth-cli download --host <isynth host>:<isynth port> --env <isynth environment> --remote-file constellations/const1.py

# Download a single file from an iSynth environment to some specific local path. `--local-path` can point to both a local directory or a specific file name
isynth-cli download --host <isynth host>:<isynth port> --env <isynth environment> --remote-file constellations/const1.py --local-path path/to/local/directory

isynth-cli download --host <isynth host>:<isynth port> --env <isynth environment> --remote-file constellations/const1.py --local-path path/to/local/directory/and/file.py

# Download a remote folder from an iSynth environment to some specific local path. `--local-path` can point to both a local directory or a specific file name.
# The result will be a single tarball (.tgz format) containing all files contained in the remote folder.
# If your `--local-path` points to a directory on your local machine and your `--remote-folder` is path/to/some/folder_to_download
# then all files will be downloaded and archived into `folder_to_download.tgz` in your `--local-path` directory.
isynth-cli download --host <isynth host>:<isynth port> --env <isynth environment> --remote-folder constellations/sub_folder --local-path path/to/local/directory

# You may download all contents from subfolders recursively by adding the `--recursive` flag:
isynth-cli download --host <isynth host>:<isynth port> --env <isynth environment> --remote-folder constellations/sub_folder --local-path path/to/local/directory --recursive

# The default behavior of this command will include all downloaded files with paths relative to the remote folder into the tarball,
# i.e. if you set `--remote-folder constellation` and a file `constellation/some_constellation.py` is downloaded
# from iSynth then it will be included as `some_constellation.py` in the resulting tarball (.tgz file).
# You may choose to include it with the full path within the iSynth environment (`constellation/some_constellation.py`)
# in the above example by adding the flag `--preserve-full-path`:
isynth-cli download --host <isynth host>:<isynth port> --env <isynth environment> --remote-folder constellations/sub_folder --local-path path/to/local/directory --preserve-full-path


```

### Mandatory Flags

```
  -e, --env string    iSynth environment (default "")
      --host string   isynth base url (e.g. http://localhost:8010).
                      If you use https protocol to connect to iSynth you may specify the
                      certificates to be used by setting the environment variable
                      'SSL_CERT_FILE' (e.g.
                      SSL_CERT_FILE=/usr/local/share/ca-certificates/ca-all.pem).
                      Alternatively you may also use the directory version of this environment
                      variable SSL_CERT_DIR, (e.g.
                      SSL_CERT_DIR=/usr/local/share/ca-certificates/) (default "")
```

### Optional Flags

```
      --local-path string      Path to the local file or directory into which you would like
                               to save the downloaded content. 
                               The current working directory will be used if you do not
                               specify this option. (default ".")
      --preserve-full-path     If this flag is set then the full path of downloaded files
                               within the iSynth environment will be preserved in the
                               resulting tarball. Otherwise files will be relative to the
                               *--remote-folder*. Can only be used together with
                               *--remote-folder*. (default false)
      --recursive              If this flag is set then the content of subfolders will be
                               downloaded recursively. Can only be used with --remote-folder.
                               (default false)
      --remote-file string     Path to the file within the iSynth environment which you wish
                               to download. Should not be a folder. (default "")
      --remote-folder string   Path to the folder within the iSynth environment which you wish
                               to download. Should not be a simple file. (default "")
```

### Technical Flags

```
      --dump-http-requests             Dump all raw HTTP requests and responses. Cannot be
                                       used together with '--silent' (default false)
      --first-retry-after int          Time until first retry in seconds. All further retries
                                       will use capped exponential backoff with jitter. (default 1)
      --force-http-1                   enforce HTTP/1.1 protocol even if the iSynth server
                                       support HTTP/2.0 (default false)
      --force-no-auth                  if set then isynth-cli will not send an authentication
                                       token to iSynth even if one has been set via login
                                       before (default false)
      --max-retries uint               maximal number of retries for REST calls in case of
                                       certain errors (default 1)
      --max-time-between-retries int   Max time between two retries in seconds (the 'capped'
                                       in capped exponential backoff with jitter). (default 30)
      --silent                         suppress ALL logging outputs besides errors. Cannot be
                                       used together with '--verbose' or
                                       '--dump-http-requests'. (default false)
      --time-between-retries int       DEPRECATED: Time until first retry in seconds. All
                                       further retries will use capped exponential backoff
                                       with jitter. Use '--first-retry-after' (default 0)
      --timeout int                    Timeout for all REST calls in seconds. Use 0 for no
                                       timeout (default 300)
      --verbose                        verbose output. Cannot be used together with
                                       '--silent'. (default false)
```

### Additional Options

```
  -h, --help   help for download (default false)
```

### SEE ALSO

* [isynth-cli](isynth-cli.md)	 - isynth-cli - a cli for invoking isynth functionality
###### Auto generated by itopia AG on 23-Feb-2025
