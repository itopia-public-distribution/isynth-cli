## isynth-cli run-order

Synthesize data in iSynth based on an iSynth order

### Synopsis

Synthesize data in iSynth based on an iSynth order.

The order may be defined using one of the following three options:
1. in a file stored on your local machine, or
2. in a file stored within your iSynth environment, or
3. inline (just as a string, not stored as a file)

```
isynth-cli run-order [flags]
```

### Examples

```bash
# Execute an order:
isynth-cli run-order --host <isynth host>:<isynth port> --env <isynth environment> --order order/path/to/order.yml

# If you have long a long running order you may run into timeout issues with the above command.
# In this case you may choose to trigger the order asynchronously in iSynth. 
# iSynth CLI will then continuously poll for the current status until the order has been finished in iSynth:

isynth-cli run-order --host <isynth host>:<isynth port> --env <isynth environment> --order order/path/to/order.yml --async

# By default the iSynth CLI will check the current status of an asynchronously started order every 5 seconds.
# You may override this interval and check every 2 seconds:

isynth-cli run-order --host <isynth host>:<isynth port> --env <isynth environment> --order order/path/to/order.yml --async --async-poll-interval 2

# iSynth will tell you the ID of your order execution (which you may need later on once you want to provision the generated data into some target systems).
# You may save the order ID into a variable like this (bash version; you may need to adjust the code if you try to use powershell):

ORDER_ID=$(isynth-cli run-order --host <isynth host>:<isynth port> --env <isynth environment> --order order/path/to/order.yml --async)

# Beware that this example shows the syntax for bash. This will not work in Powershell.
# If you are on Windows you may either use this example with WSL / Git Bash or you will need to adjust the syntax slightly to Powershell.

# Instead of executing an order file which already exists in your iSynth environment, you may also  
# use a local order file on your machine. iSynth CLI will then send the information from your local order file to iSynth: 
isynth-cli run-order --host <isynth host>:<isynth port> --env <isynth environment> --local-order-file ./orders/my_local_order.yml

# Lastly you can also use a string value containing the actual order data in the iSynth format.

# An order in json format:
isynth-cli run-order --host <isynth host>:<isynth port> --env <isynth environment> --order-data '{"label": "data for development", "items": [{"constellation": "module_a.constellation","copies": 10},{"constellation": "module_b.constellation", "copies": 30 }] }'

# It might be better for your eyes if you use a separate variable for your order data:
order_data= '{ 
	"label": "data for development", 
	"items": [ 
		{ 
			"constellation": "module_a.constellation", 
			"copies": 10 
		}, 
		{ 
			"constellation": "module_b.constellation", 
			"copies": 30 
		} 
	] 
}'

isynth-cli run-order --host <isynth host>:<isynth port> --env <isynth environment> --order-data "$order_data"

# Please note the quotation marks around the variable $order_data.
# You may also use a yaml format (instead of json).
```

### Mandatory Flags

```
  -e, --env string    iSynth environment (default "")
      --host string   isynth base url (e.g. http://localhost:8010).
                      If you use https protocol to connect to iSynth you may specify the
                      certificates to be used by setting the environment variable
                      'SSL_CERT_FILE' (e.g.
                      SSL_CERT_FILE=/usr/local/share/ca-certificates/ca-all.pem).
                      Alternatively you may also use the directory version of this environment
                      variable SSL_CERT_DIR, (e.g.
                      SSL_CERT_DIR=/usr/local/share/ca-certificates/) (default "")
```

### Optional Flags

```
      --async                         If this flag is set then the iSynth order will be
                                      started asynchronously and the status will be polled
                                      regularly. 
                                      This may help against timeout issues with long running
                                      orders. Needs at least iSynth 2.7 (default false)
      --async-poll-interval float32   Number of seconds between two status polls for an
                                      asynchronously running order (default 5)
  -f, --local-order-file string       Path to the order file on your local machine. Cannot be
                                      used together with *order-data* or *order*. (default "")
  -o, --order string                  Path to the order file within the iSynth environment.
                                      Cannot be used together with *order-data* or
                                      *local-order-file*. (default "")
  -d, --order-data string             iSynth order data (json or yaml format). Requires iSynth
                                      version >= 2.7. Cannot be used together with *order* or
                                      *local-order-file*. (default "")
```

### Technical Flags

```
      --dump-http-requests             Dump all raw HTTP requests and responses. Cannot be
                                       used together with '--silent' (default false)
      --first-retry-after int          Time until first retry in seconds. All further retries
                                       will use capped exponential backoff with jitter. (default 1)
      --force-http-1                   enforce HTTP/1.1 protocol even if the iSynth server
                                       support HTTP/2.0 (default false)
      --force-no-auth                  if set then isynth-cli will not send an authentication
                                       token to iSynth even if one has been set via login
                                       before (default false)
      --max-retries uint               maximal number of retries for REST calls in case of
                                       certain errors (default 1)
      --max-time-between-retries int   Max time between two retries in seconds (the 'capped'
                                       in capped exponential backoff with jitter). (default 30)
      --silent                         suppress ALL logging outputs besides errors. Cannot be
                                       used together with '--verbose' or
                                       '--dump-http-requests'. (default false)
      --time-between-retries int       DEPRECATED: Time until first retry in seconds. All
                                       further retries will use capped exponential backoff
                                       with jitter. Use '--first-retry-after' (default 0)
      --timeout int                    Timeout for all REST calls in seconds. Use 0 for no
                                       timeout (default 300)
      --verbose                        verbose output. Cannot be used together with
                                       '--silent'. (default false)
```

### Additional Options

```
  -h, --help   help for run-order (default false)
```

### SEE ALSO

* [isynth-cli](isynth-cli.md)	 - isynth-cli - a cli for invoking isynth functionality
###### Auto generated by itopia AG on 23-Feb-2025
