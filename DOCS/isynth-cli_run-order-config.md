## isynth-cli run-order-config

⚠ DEPRECATED!!!!! ⚠ Synthesize data in iSynth based on an iSynth order config

### Synopsis

⚠ THIS COMMAND IS DEPRECATED AS OF ISYNTH 4.0 AND WILL STOP WORKING WITH ISYNTH 4.2!!! Change your code to use data configs. ⚠
	
Synthesize data in iSynth based on an iSynth order config.
Please note that your environment must have support for order configs implemented.

The order config may be defined:
1. in a file stored on your local machine, or
2. in a file stored within your iSynth environment, or
3. inline (just as a string, not stored as a file)

Options 1 and 3 will (under the hood) create an order config file within your iSynth environment. You will need to define the desired *order context* (i.e. subfolder of the order_config folder in your environment).
Don't forget to activate the chosen order context in your settings.yml

```
isynth-cli run-order-config [flags]
```

### Examples

```bash
# Execute an order config stored inside your iSynth environment:
isynth-cli run-order-config --host <isynth host>:<isynth port> --env <isynth environment> --order-config order_config/path/to/order_config.yml

# iSynth will tell you the ID of your order execution (which you may need later on once you want to provision the generated data into some target systems).
# You may save the order ID into a variable like this (bash version; you may need to adjust the code if you try to use powershell):

ORDER_ID=$(isynth-cli run-order-config --host <isynth host>:<isynth port> --env <isynth environment> --order-config order_config/path/to/order_config.yml)

# Beware that this example shows the syntax for bash. This will not work in Powershell.
# If you are on Windows you may either use this example with WSL / Git Bash or you will need to adjust the syntax slightly to Powershell.

# Instead of executing an order config file which already exists in your iSynth environment, you may also  
# use a local order config file on your machine. iSynth CLI will then send the information from your local order config file to iSynth. 
# You will have to define the order context to use (i.e. the subfolder of the folder order_config inside your environment). # This folder should already exist in your iSynth environment and it should be an activated order context in your settings.yml. 
# You should also define a label / name for the execution of your order config. You will find this name in the iSynth UI on the order overview page. 
isynth-cli run-order-config --host <isynth host>:<isynth port> --env <isynth environment> --local-order-config ./order-config/my_local_order-config.yml --order-config-context PIPELINE --order-label my_test_execution

# Lastly you can also use a string value containing the actual order config data in the iSynth format. All other explanations for usage of local order config files apply in this case as well.

# An order config in yaml format using a bash variable:
order_config_data='case_1:
		based_on: client_templates/individual_client
		num_copies: 20
		variant_parameters:
		  param1: value1
		  param2: 2-CH
		  param3: 4
	  ca_se_2:
		based_on: client_templates/couple_client
		variant_parameters:
		  param1: value2
		modify:
		  Constellation:
			readonly: true'

isynth-cli run-order-config --host <isynth host>:<isynth port> --env <isynth environment> --order-config-data "$order_config_data" --order-config-context PIPELINE --order-label my_test_execution

# Please note the quotation marks around the variable $order_config_data.
# You may also use a json format (instead of yaml). You may also pass the content to the CLI without using a bash variable.
```

### Mandatory Flags

```
  -e, --env string    iSynth environment (default "")
      --host string   isynth base url (e.g. http://localhost:8010).
                      If you use https protocol to connect to iSynth you may specify the
                      certificates to be used by setting the environment variable
                      'SSL_CERT_FILE' (e.g.
                      SSL_CERT_FILE=/usr/local/share/ca-certificates/ca-all.pem).
                      Alternatively you may also use the directory version of this environment
                      variable SSL_CERT_DIR, (e.g.
                      SSL_CERT_DIR=/usr/local/share/ca-certificates/) (default "")
```

### Optional Flags

```
      --async                         If this flag is set then the iSynth order will be
                                      started asynchronously and the status will be polled
                                      regularly. 
                                      	This may help against timeout issues with long running
                                      orders. Needs at least iSynth 2.7 (default false).
                                      (default false)
      --async-poll-interval float32   Number of seconds between two status polls for an
                                      asynchronously running order (default 5)
      --local-order-config string     Path to the order config file locally on your machine.
                                      You must also set *order-config-context* if you use this
                                      option. (default "")
      --order-config string           Path to the order config file within the iSynth
                                      environment (default "")
      --order-config-context string   Order config context (i.e. existing subfolder of
                                      *order_config* in your iSynth environment) (default "")
      --order-config-data string      Order config data as string. You must also set
                                      *order-config-context* if you use this option. (default "")
      --order-label string            label of the iSynth order. You will see this name under
                                      the order overview in the iSynth web UI. 
                                      Mandatory if you use *order-config-data* (order config
                                      as string in memory) or *local-order-config* (order
                                      config from a local file).
                                      Must not be set if you use *order-config* (existing
                                      order config within your iSynth environment). (default "")
```

### Technical Flags

```
      --dump-http-requests             Dump all raw HTTP requests and responses. Cannot be
                                       used together with '--silent' (default false)
      --first-retry-after int          Time until first retry in seconds. All further retries
                                       will use capped exponential backoff with jitter. (default 1)
      --force-http-1                   enforce HTTP/1.1 protocol even if the iSynth server
                                       support HTTP/2.0 (default false)
      --force-no-auth                  if set then isynth-cli will not send an authentication
                                       token to iSynth even if one has been set via login
                                       before (default false)
      --max-retries uint               maximal number of retries for REST calls in case of
                                       certain errors (default 1)
      --max-time-between-retries int   Max time between two retries in seconds (the 'capped'
                                       in capped exponential backoff with jitter). (default 30)
      --silent                         suppress ALL logging outputs besides errors. Cannot be
                                       used together with '--verbose' or
                                       '--dump-http-requests'. (default false)
      --time-between-retries int       DEPRECATED: Time until first retry in seconds. All
                                       further retries will use capped exponential backoff
                                       with jitter. Use '--first-retry-after' (default 0)
      --timeout int                    Timeout for all REST calls in seconds. Use 0 for no
                                       timeout (default 300)
      --verbose                        verbose output. Cannot be used together with
                                       '--silent'. (default false)
```

### Additional Options

```
  -h, --help   help for run-order-config (default false)
```

### SEE ALSO

* [isynth-cli](isynth-cli.md)	 - isynth-cli - a cli for invoking isynth functionality
###### Auto generated by itopia AG on 23-Feb-2025
