## isynth-cli init

Initialize the environment (generate model and reset environment) - DELETES ALL PREVIOUSLY SYNTHESIZED DATA FROM YOUR ISYNTH ENVIRONMENT DATABASE

### Synopsis

Initialize your isynth environment, i.e. perform one or more of the following tasks (depending on the flags that you set):
- Generate the iSynth model (transform iSynth model language into Django code)
- Reset the iSynth environment, i.e. physically create database tables according to your iSynth model. This step will always be performed if you call "init" and will DELETE ALL YOUR PREVIOUSLY SNYTHESIZED DATA FROM YOUR ISYNTH ENVIRONMENT DATABASE.
- Refresh constellation infos: this will create some metadata which will help the iSynth UI to give you better support

```
isynth-cli init [flags]
```

### Examples

```bash
# generate the model and reset iSynth environment (the usual action after iSynth installation in a pipeline):
isynth-cli init --host <isynth host>:<isynth port> --env <isynth environment>

# just reset the iSynth environment (if you just want to reset the database, e.g. for a clean test setup, but you don't need to re-generate the model):
isynth-cli init --host <isynth host>:<isynth port> --env <isynth environment> --reset-only

# Fully initialize the environment (generate model, reset environment and refresh constellation infos):
isynth-cli init --host <isynth host>:<isynth port> --env <isynth environment> --full

# By default, a reset of the environment will also reset the iSynth counters to their initial values. Use `--preserve-counters` if you want to keep the counter values:
isynth-cli init --host <isynth host>:<isynth port> --env <isynth environment> --preserve-counters
```

### Mandatory Flags

```
  -e, --env string    iSynth environment (default "")
      --host string   isynth base url (e.g. http://localhost:8010).
                      If you use https protocol to connect to iSynth you may specify the
                      certificates to be used by setting the environment variable
                      'SSL_CERT_FILE' (e.g.
                      SSL_CERT_FILE=/usr/local/share/ca-certificates/ca-all.pem).
                      Alternatively you may also use the directory version of this environment
                      variable SSL_CERT_DIR, (e.g.
                      SSL_CERT_DIR=/usr/local/share/ca-certificates/) (default "")
```

### Optional Flags

```
  -f, --full                Fully initialize environment (as opposed to a simple refresh).
                            Cannot be used together with --reset-only. (default false)
      --preserve-counters   Preserve current iSynth counter values, i.e. do not reset them to
                            their initial values. (default false)
      --reset-only          Only reset the environment (i.e. do not generate the model or
                            refresh constellation infos). Cannot be used together with --full.
                            (default false)
```

### Technical Flags

```
      --dump-http-requests             Dump all raw HTTP requests and responses. Cannot be
                                       used together with '--silent' (default false)
      --first-retry-after int          Time until first retry in seconds. All further retries
                                       will use capped exponential backoff with jitter. (default 1)
      --force-http-1                   enforce HTTP/1.1 protocol even if the iSynth server
                                       support HTTP/2.0 (default false)
      --force-no-auth                  if set then isynth-cli will not send an authentication
                                       token to iSynth even if one has been set via login
                                       before (default false)
      --max-retries uint               maximal number of retries for REST calls in case of
                                       certain errors (default 1)
      --max-time-between-retries int   Max time between two retries in seconds (the 'capped'
                                       in capped exponential backoff with jitter). (default 30)
      --silent                         suppress ALL logging outputs besides errors. Cannot be
                                       used together with '--verbose' or
                                       '--dump-http-requests'. (default false)
      --time-between-retries int       DEPRECATED: Time until first retry in seconds. All
                                       further retries will use capped exponential backoff
                                       with jitter. Use '--first-retry-after' (default 0)
      --timeout int                    Timeout for all REST calls in seconds. Use 0 for no
                                       timeout (default 300)
      --verbose                        verbose output. Cannot be used together with
                                       '--silent'. (default false)
```

### Additional Options

```
  -h, --help   help for init (default false)
```

### SEE ALSO

* [isynth-cli](isynth-cli.md)	 - isynth-cli - a cli for invoking isynth functionality
###### Auto generated by itopia AG on 23-Feb-2025
