## isynth-cli execute-plugin

Execute an iSynth plugin

```
isynth-cli execute-plugin [flags]
```

### Examples

```bash
# Plugin without arguments:
isynth-cli execute-plugin --host <isynth host>:<isynth port> --env <isynth environment> --plugin-name name_from_plugin_defintion --plugin-module plugins.submodule.file_with_plugin_code_without_extension --path path/to/file/on/which/you/want/to/apply/the/plugin
# Plugin with arguments:
isynth-cli execute-plugin --host <isynth host>:<isynth port> --env <isynth environment> --plugin-name name_from_plugin_defintion --plugin-module plugins.submodule.file_with_plugin_code_without_extension --path path/to/file/on/which/you/want/to/apply/the/plugin --plugin-arguments '{"arg1":"value1", "arg2":"value2"}'
# As of iSynth 3.0 plugins may also return a structured response (JSON format) rather than a mere string. CLI will return the JSON string to the caller. If you are interested in only one specific attribute of this JSON structure
# you may use the --json-path flag. Let's assume the iSynth plugin returns the following structure:
# { "key1": "value1", "nested_key2": {"key2_1":"value2_1", "key2_2":"value2_2"}}
# then use the following command to receive the value of key2_2:
isynth-cli execute-plugin --host <isynth host>:<isynth port> --env <isynth environment> --plugin-name name_from_plugin_defintion --plugin-module plugins.submodule.file_with_plugin_code_without_extension --path path/to/file/on/which/you/want/to/apply/the/plugin --json-path nested_key2.key2_2
# this will return value2_2. Please note that at the moment --json-path does support only simple paths. More complex features like support for lists may be implemented in the future.
```

### Mandatory Flags

```
  -e, --env string             iSynth environment (default "")
      --host string            isynth base url (e.g. http://localhost:8010).
                               If you use https protocol to connect to iSynth you may specify
                               the certificates to be used by setting the environment variable
                               'SSL_CERT_FILE' (e.g.
                               SSL_CERT_FILE=/usr/local/share/ca-certificates/ca-all.pem).
                               Alternatively you may also use the directory version of this
                               environment variable SSL_CERT_DIR, (e.g.
                               SSL_CERT_DIR=/usr/local/share/ca-certificates/) (default "")
      --plugin-module string   module name of the iSynth plugin (default "")
      --plugin-name string     name of the iSynth plugin (default "")
```

### Optional Flags

```
      --json-path string          select a specific value from a JSON reply from iSynth.
                                  Requires a plugin that returns a structured response. See
                                  the examples for futher details. (default "")
      --path string               path to which the iSynth plugin should be applied. Don't set
                                  if you want to run the plugin on the environment (default "")
      --plugin-arguments string   arguments to pass to the iSynth Plugin in JSON format
                                  (default "")
```

### Technical Flags

```
      --dump-http-requests             Dump all raw HTTP requests and responses. Cannot be
                                       used together with '--silent' (default false)
      --first-retry-after int          Time until first retry in seconds. All further retries
                                       will use capped exponential backoff with jitter. (default 1)
      --force-http-1                   enforce HTTP/1.1 protocol even if the iSynth server
                                       support HTTP/2.0 (default false)
      --force-no-auth                  if set then isynth-cli will not send an authentication
                                       token to iSynth even if one has been set via login
                                       before (default false)
      --max-retries uint               maximal number of retries for REST calls in case of
                                       certain errors (default 1)
      --max-time-between-retries int   Max time between two retries in seconds (the 'capped'
                                       in capped exponential backoff with jitter). (default 30)
      --silent                         suppress ALL logging outputs besides errors. Cannot be
                                       used together with '--verbose' or
                                       '--dump-http-requests'. (default false)
      --time-between-retries int       DEPRECATED: Time until first retry in seconds. All
                                       further retries will use capped exponential backoff
                                       with jitter. Use '--first-retry-after' (default 0)
      --timeout int                    Timeout for all REST calls in seconds. Use 0 for no
                                       timeout (default 300)
      --verbose                        verbose output. Cannot be used together with
                                       '--silent'. (default false)
```

### Additional Options

```
  -h, --help   help for execute-plugin (default false)
```

### SEE ALSO

* [isynth-cli](isynth-cli.md)	 - isynth-cli - a cli for invoking isynth functionality
###### Auto generated by itopia AG on 23-Feb-2025
